from flask import Flask, request, jsonify, render_template
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from surprise import Dataset, Reader, SVD, KNNWithMeans
from surprise.model_selection import train_test_split
from collections import defaultdict

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['DEBUG'] = True


def get_all_genres():
    unique_genres = set()
    for genre_block in get_movies().genre.values:
        unique_genres |= set(genre_block.split('|'))
    return sorted(list(unique_genres))


def get_ratings():
    return pd.read_csv('./data/ratings.dat',
                       delimiter="::",
                       header=0,
                       names=['user_id', 'movie_id', 'rating', 'timestamp'],
                       engine='python')


def get_movies():
    return pd.read_csv('./data/movies.dat',
                       delimiter="::",
                       header=0,
                       names=['id', 'name', 'genre'],
                       engine='python')


def get_extended_movie_df():
    df = get_movies()
    df['year'] = df.name.apply(lambda x: x[-5:-1])
    df['name'] = df.name.apply(lambda x: x[:-7])

    unique_genres = get_all_genres()
    for genre in list(unique_genres):
        df[genre] = df.genre.apply(lambda x: int(genre in x))
    df = df.drop(columns='genre')

    ratings_df = get_ratings().groupby('movie_id').agg(
        rating_count=('rating', 'size'),
        rating_avg=('rating', 'mean'),
        rating_std=('rating', 'std'),
        timestamp_avg=('timestamp', 'mean'),
        timestamp_std=('timestamp', 'std'))

    return df.set_index('id').join(ratings_df)


def get_genre_recommendations(genre):
    movie_df = get_extended_movie_df()
    genre_matrix = movie_df[get_all_genres()].values
    genre_embeddings = PCA(n_components=4).fit_transform(genre_matrix.T)
    genre_emb_df = pd.DataFrame(get_all_genres(), columns=['genre'])
    genre_emb_df['emb'] = tuple(genre_embeddings)
    movie_genre_embeddings = np.matmul(
        (movie_df[get_all_genres()].values / np.sum(movie_df[get_all_genres()].values, axis=1).reshape(-1, 1)),
        genre_embeddings)
    movie_df['genre_embedding'] = tuple(movie_genre_embeddings)

    pure_genre_emb = np.array(list(genre_emb_df[genre_emb_df.genre == genre].emb)).reshape(-1, )
    genre_df = movie_df.copy()
    movies_genre_hard = genre_df[genre_df[genre] == 1].copy()
    avg_genre_embedding = np.mean(np.stack(movies_genre_hard.genre_embedding.apply(np.array).values), axis=0)
    emb_target = (avg_genre_embedding + pure_genre_emb) / 2
    genre_df['avg_emb_dist'] = movies_genre_hard.genre_embedding.apply(
        lambda x: np.sum((np.array(list(x)) - emb_target) ** 2))

    genre_df['rating_count_z'] = (genre_df.rating_count - genre_df.rating_count.mean()) / genre_df.rating_count.std()
    genre_df['avg_emb_dist_z'] = (genre_df.avg_emb_dist - genre_df.avg_emb_dist.mean()) / genre_df.avg_emb_dist.std()
    genre_df['rating_avg_z'] = (genre_df.rating_avg - genre_df.rating_avg.mean()) / genre_df.rating_avg.std()
    genre_df['timestamp_avg_z'] = (genre_df.timestamp_avg - genre_df.timestamp_avg.mean()) / genre_df.timestamp_avg.std()
    genre_df = genre_df[genre_df[genre] == 1]
    genre_df['movie_id'] = genre_df.index

    genre_df['ranking_score'] = genre_df.rating_count_z + genre_df.rating_avg_z + -genre_df.avg_emb_dist_z * 2.0 + genre_df.timestamp_avg_z * 0.33

    return genre_df.sort_values('ranking_score', ascending=False).head(20).to_dict('records')


def get_rating_recommendations(new_user_df):
    all_ratings = get_ratings().drop(columns=["timestamp"])
    all_ratings = pd.concat([all_ratings, new_user_df], ignore_index=True)
    reader = Reader(rating_scale=(1, 5))
    dataset = Dataset.load_from_df(all_ratings[["user_id", "movie_id", "rating"]], reader)

    svd = SVD(n_factors=12, n_epochs=5, lr_all=0.03)
    svd.fit(dataset.build_full_trainset())

    movie_ids = list(all_ratings.movie_id.unique())
    user_id = 8888
    user_ratings = []
    for movie_id in movie_ids:
        if movie_id in list(new_user_df.movie_id.values):
            continue
        pred = svd.predict(user_id, movie_id)
        user_ratings.append((movie_id, pred.est))
    user_ratings.sort(key=lambda x: x[1], reverse=True)

    rec_movie_ids = [x[0] for x in user_ratings[:20]]
    movies = get_extended_movie_df()
    movies = movies[movies.index.isin(rec_movie_ids)]
    return movies


@app.route('/rate')
def rate():
    movies = get_extended_movie_df().sample(n=100, weights='rating_count')
    movies['movie_id'] = movies.index
    return render_template('rate.html', movies=movies.to_dict('records'))


@app.route('/recommendations', methods=['POST'])
def recommendations():
    user_ids = []
    movie_ids = []
    ratings = []
    for movie_id, rating in request.form.items():
        movie_ids.append(int(movie_id))
        ratings.append(int(rating))
        user_ids.append(8888)
    new_user_df = pd.DataFrame.from_dict({'user_id': user_ids, 'movie_id':movie_ids, 'rating': ratings})
    #return str(new_user_df)
    movies = get_rating_recommendations(new_user_df)
    movies['movie_id'] = movies.index
    return render_template('recommendations.html', movies=movies.to_dict('records'), user_df=str(new_user_df))


@app.route('/genre/<genre>')
def genre_recommend(genre):
    movies = get_genre_recommendations(genre)
    return render_template('genre_recommend.html', genre=genre, movies=movies)


@app.route('/')
def index():
    genres = get_all_genres()
    return render_template('index.html', genres=genres)


if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000, debug=True)
